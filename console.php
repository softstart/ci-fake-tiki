<?php

require __DIR__ . '/vendor_bundled/vendor/autoload.php';

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class BaseCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:create-user';

    protected function configure()
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
	$output->writeln('Executing ' . static::$defaultName);
    }
}


$application = new Application();

//database:install
class DBInstallCMD extends BaseCommand {
	protected static $defaultName = 'database:install';
}

$application->add(new DBInstallCMD('database:install'));

//cache:generate
class CacheGenerateCMD extends BaseCommand {
	protected static $defaultName = 'cache:generate';
}

$application->add(new CacheGenerateCMD('cache:generate'));

//index:rebuild
class IndexRebuildCMD extends BaseCommand {
	protected static $defaultName = 'index:rebuild';
}

$application->add(new IndexRebuildCMD('index:rebuild'));

//cache:clear
class CacheCleanCMD extends BaseCommand {
	protected static $defaultName = 'cache:clear';
}

$application->add(new CacheCleanCMD('cache:clear'));

//database:update
class DBUpdateCMD extends BaseCommand {
	protected static $defaultName = 'database:update';
}

$application->add(new DBUpdateCMD('database:update'));

$application->run();

